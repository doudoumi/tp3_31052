/*  INF3105 - Structures de données et algorithmes
    Lab 11 -- Graphes / Représentations et algorithmes de base
    http://ericbeaudry.uqam.ca/INF3105/lab11/
*/
#ifndef GRAPHE_H
#define GRAPHE_H

#include <queue>
#include <map>
#include <set>
#include <iostream>

// Le type S est le type utilisé pour identifier les sommets
template <class S>
class Graphe {
  public:
    // Interface publique pour créer le graphe.
    void ajouterSommet(const S& s);
    void ajouterAreteOrientee(const S& s1, const S& s2);
    void ajouterAreteNonOrientee(const S& s1, const S& s2);

    // Interface publique pour les requêtes de base.
    // Amélioration possible : retourner le résultat au lieu de faire l'affichage à l'interne.
    void parcoursRechercheProfondeur(const S& s) const;
    void parcoursRechercheLargueur(const S& s) const;
    void extraireComposantesConnexes() const;

  private:
    struct Sommet {
        std::set<S> voisins; // ensemble des sommets accessibles à partir du sommet.
        mutable bool estVisite;
    };
    std::map<S, Sommet> sommets; // identification --> sommet

    void resetMutables() const;
    void parcoursProfondeurRec(const S& s) const;
};

template <class S>
void Graphe<S>::ajouterSommet(const S& s) {
  sommets[s];
}

template <class S>
void Graphe<S>::ajouterAreteNonOrientee(const S& s1, const S& s2) {
  ajouterAreteOrientee(s1, s2);
  ajouterAreteOrientee(s2, s1);
}

template <class S>
void Graphe<S>::ajouterAreteOrientee(const S& s1, const S& s2) {
  sommets[s1].voisins.insert(s2);
}

template <class S>
void Graphe<S>::parcoursProfondeurRec(const S& s) const {
  sommets.at(s).estVisite = true;
  std::cout << s << " ";

  for(const S& voisin : sommets.at(s).voisins)
    if(!sommets.at(voisin).estVisite)
      parcoursProfondeurRec(voisin);
}

template <class S>
void Graphe<S>::parcoursRechercheProfondeur(const S& s) const {
  resetMutables();
  parcoursProfondeurRec(s);
  std::cout << std::endl;
}

template <class S>
void Graphe<S>::parcoursRechercheLargueur(const S& s) const {
  resetMutables();
  std::queue<S> file;
  sommets.at(s).estVisite = true;
  file.push(s);
  while(!file.empty()) {
    S elem = file.front();
    file.pop();
    std::cout << elem << " ";
    const Sommet& sommet = sommets.at(elem);

    for(const S& voisin : sommet.voisins)
      if(!sommets.at(voisin).estVisite) {
        sommets.at(voisin).estVisite = true;
        file.push(voisin);
      }
  }
  std::cout << std::endl;
}

template <class S>
void Graphe<S>::extraireComposantesConnexes() const {
  resetMutables();
  std::cout << "{ ";
  for(const auto& pair : sommets) {
    if(pair.second.estVisite)
      continue;
    std::cout << "{ ";
    std::queue<S> file;
    pair.second.estVisite = true;
    file.push(pair.first);
    while(!file.empty()) {
      S elem = file.front();
      file.pop();
      std::cout << elem << " ";
      const Sommet& sommet = sommets.at(elem);

      for(const S& voisin : sommet.voisins)
        if(!sommets.at(voisin).estVisite) {
          sommets.at(voisin).estVisite = true;
          file.push(voisin);
        }
    }
    std::cout << "}";
  }
  std::cout << " }" << std::endl;
}

template <class S>
void Graphe<S>::resetMutables() const {
  for(auto& pair : sommets)
    pair.second.estVisite = false;
}

#endif
