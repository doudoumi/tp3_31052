/*
*Nom :BROURI
*Prénom :Adem
*
*Code permanent :BROA23089702
*/
#include "DocumentXML.h"
#include "Histoire.h"
#include <math.h>
#include <iomanip>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "traitement.h"

using namespace std;
vector<Histoire *> *lireDocuments(string a_nomFichier)
{
    vector<Histoire *> *histoires = new vector<Histoire *>();
    DocumentXML *listeFichiers = lireFichierXML(a_nomFichier);

    Element *courrant = listeFichiers->racine();
    vector<Contenu *>::const_iterator it = courrant->begin();

    // trouver <liste>
    while (!(*it)->estElement())
        ++it;
    courrant = (Element *)(*it);

    for (Contenu *contenu : *courrant)
    {
        if (contenu->estElement())
        {
            Element *element = (Element *)contenu;

            DocumentXML *doc = lireFichierXML(element->attribut(string("fichier")));

            vector<Histoire *> *h = extraireHistoires(*doc);

            histoires->insert(histoires->end(), h->begin(), h->end());
        }
    }

    return histoires;
}

bool estCaractereMot(char c);

int main(int argc,char * argv[])
{
    int index = 0;
    string titre;
    vector<Histoire *> *histoires = lireDocuments(string("listeDocument.xml"));
    cout << "Entrée le titre de l'histoire:";
    getline(cin, titre);
    titre.erase(remove(titre.begin(), titre.end(), ' '), titre.end());
    if (titre.find('(') != -1)
    {
    traitement_partie(histoires,titre);
    }
    else
    {
        index = histoire_courante_index(histoires, titre);
        traitement_graphe(histoires, index);
    }
    return 0;
}
/*for (int i=0; (i<histoires->size());++i)
    {
        cout << histoires->at(i)->titre();
    }*/