#ifndef TRAITEMENT_H
#define TRAITEMENT_H


#include <vector>
#include <algorithm>
#include "Histoire.h"


////////Fonction privé regardé le fichier .cpp//////////
int histoire_courante_index(vector<Histoire *> *hist, string const &titre);

///////Fonction privé regardé le fichier .cpp//////////
void traitement_graphe(vector<Histoire *> *hist, int index);

///////Fonction privé regardé le fichier .cpp//////////
void traitement_graphe2(vector<Histoire *> *hist, int index,int index2);

///////Fonction privé regardé le fichier .cpp//////////
void traitement_partie(vector<Histoire *> *hist,string const &titre);

///////Fonction privé regardé le fichier .cpp//////////
Phrase min_local(map<Phrase, double> const &Distance);

///////Fonction privé regardé le fichier .cpp//////////
map<Phrase, double> Distance_map(vector<Histoire *> *hist, Phrase p,Phrase p1,int index);

///////Fonction privé regardé le fichier .cpp//////////
map<Phrase, double> Distance_map2(vector<Histoire *> *hist, Phrase p, Phrase p1, int index,int index2);

#endif