#include "traitement.h"

/*
*Traite les données pour une histoire simple.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param index   int,un entier qui indique la position de l'histoire choisie par l'utilisateur.
*PS:l'implémentation avec le garphe.h du laboratoire complique l'utilisation des distances.
*/
void traitement_graphe(vector<Histoire *> *hist, int index)
{
    map<Phrase, double> Sommet_debut;
    map<Phrase, double> Sommet_p1;
    auto Phrase_1 = hist->at(index)->begin();
    for (auto it = (hist->at(index)->begin() + 1); it != hist->at(index)->end(); ++it)
        Sommet_debut[*it] = Phrase_1->distance(*it);
    Phrase p1 = min_local(Sommet_debut);
    for (auto ite = (hist->at(index)->begin()); ite != hist->at(index)->end(); ++ite)
        if (ite->Get_phrase() != hist->at(index)->begin()->Get_phrase())
            Sommet_p1[*ite] = p1.distance(*ite);
    Phrase p2 = min_local(Sommet_p1);

    Phrase p3 = min_local(Distance_map(hist, p1, p2, index));

    Phrase p4 = min_local(Distance_map(hist, p2, p3, index));
    cout << "Le résumer de " << hist->at(index)->titre() << " est :" << endl;
    cout << hist->at(index)->begin()->Get_phrase() << endl
         << p1.Get_phrase() << endl
         << p2.Get_phrase() << endl
         << p3.Get_phrase() << endl
         << p4.Get_phrase() << endl
         << (hist->at(index)->end() - 2)->Get_phrase() << "." << endl;
}
/*
*Donne la phrase avec une distance minimal
*
*@param Distance map<Phrase, double>,un map qui contient des distances.
*
*@return min->first Phrase,la phrase avec la plus petite distance dans le map.
*/
Phrase min_local(map<Phrase, double> const &Distance)
{
    auto it = Distance.begin();
    auto min = Distance.begin();
    while (it != Distance.end())
    {
        if (it->second != 0)
        {
            if (it->second < min->second)
            {
                min = it;
            }
        }
        ++it;
    }
    return min->first;
}
/*
*Traite les données pour une histoire partitioner.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param index   int,un entier qui indique la position de l'histoire choisie par l'utilisateur.
*@param index   int,un entier qui indique la position de la partie 2 de l'histoire choisie par l'utilisateur.
*/
void traitement_graphe2(vector<Histoire *> *hist, int index, int index2)
{
    map<Phrase, double> Sommet_debut;
    map<Phrase, double> Sommet_p1;
    auto Phrase_1 = hist->at(index)->begin();
    for (auto it = (hist->at(index)->begin() + 1); it != hist->at(index)->end(); ++it)
        Sommet_debut[*it] = Phrase_1->distance(*it);
    for (auto iter = (hist->at(index2)->begin() + 1); iter != hist->at(index2)->end(); ++iter)
        Sommet_debut[*iter] = Phrase_1->distance(*iter);    
    Phrase p1 = min_local(Sommet_debut);
    for (auto ite = (hist->at(index)->begin()); ite != hist->at(index)->end(); ++ite)
        if (ite->Get_phrase() != hist->at(index)->begin()->Get_phrase())
            Sommet_p1[*ite] = p1.distance(*ite);
    for (auto itera = (hist->at(index)->begin()); itera != hist->at(index)->end(); ++itera)
        if (itera->Get_phrase() != hist->at(index)->begin()->Get_phrase())
            Sommet_p1[*itera] = p1.distance(*itera);
    Phrase p2 = min_local(Sommet_p1);

    Phrase p3 = min_local(Distance_map2(hist, p1, p2, index,index2));

    Phrase p4 = min_local(Distance_map2(hist, p2, p3, index,index2));
    cout << "Le résumer de " << hist->at(index)->titre() << " and " << hist->at(index2)->titre() << " est :" << endl;
    cout << hist->at(index)->begin()->Get_phrase() << endl
         << p1.Get_phrase() << endl
         << p2.Get_phrase() << endl
         << p3.Get_phrase() << endl
         << p4.Get_phrase() << endl
         << (hist->at(index2)->end() - 2)->Get_phrase() << "." << endl;
}
/*
*Crée un map contenant les distances relative à une phrase.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param index   int,un entier qui indique la position de l'histoire choisie par l'utilisateur.
*@param p1   Phrase,la phrase précédante à partir de laquelle on calcul la distance.
*@param p    Phrase,l'ancienne phrase la plus proche que l'on ne veut pas réinclure.
*
*@return Sommet_p1   map<Phrase, double>,le map contenant les distances relative à une phrase.
*/
map<Phrase, double> Distance_map(vector<Histoire *> *hist, Phrase p, Phrase p1, int index)
{
    map<Phrase, double> Sommet_p1;
    for (auto it = (hist->at(index)->begin()); it != hist->at(index)->end(); ++it)
        if (it->Get_phrase() != p.Get_phrase())
            Sommet_p1[*it] = p1.distance(*it);
    return Sommet_p1;
}
/*
*Permet de déterminé l'index de l'histoire choisie.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param titre string,le nom de l'histoire entrée par l'utilisateur.
*
*@return histoire_index int,la position de notre histoire dans le vector.
*/
int histoire_courante_index(vector<Histoire *> *hist, string const &titre)
{
    int histoire_index = 0;
    bool trouve = false;
    while ((histoire_index < (hist->size() - 1)) && !(trouve))
    {
        string histoire_courante = hist->at(histoire_index)->titre();
        histoire_courante.erase(remove(histoire_courante.begin(), histoire_courante.end(), ' '), histoire_courante.end());
        if (titre == histoire_courante)
        {
            trouve = true;
        }
        ++histoire_index;
    }
    --histoire_index;
    return histoire_index;
}
/*
*Traite les histoires définis par parties.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param titre string,le nom de l'histoire entrée par l'utilisateur.
*
*/
void traitement_partie(vector<Histoire *> *hist, string const &titre)
{
    int index2 = 0, index = 0;

    string titre_ajout1 = "(partie2)", titre_ajout2 = "(partie1)", temp, titre_partie2;
    for (int i = 0; (i < titre.size()) && (titre.at(i) != '('); ++i)
    {
        temp += titre.at(i);
    }
    if (titre.find('1') != -1)
    {
        titre_partie2 = temp + titre_ajout1;
        titre_partie2.erase(remove(titre_partie2.begin(), titre_partie2.end(), ' '), titre_partie2.end());
        index = histoire_courante_index(hist, titre);
        index2 = histoire_courante_index(hist, titre_partie2);
    }
    else
    {
        titre_partie2 = temp + titre_ajout2;
        titre_partie2.erase(remove(titre_partie2.begin(), titre_partie2.end(), ' '), titre_partie2.end());
        index2 = histoire_courante_index(hist, titre);
        index = histoire_courante_index(hist, titre_partie2);
    }
    traitement_graphe2(hist, index, index2);

}
/*
*Crée un map contenant les distances relative à une phrase.
*
*@param *hist vector<Histoire *>,un pointeur vers un vector de pointeur d'histoires.
*@param index   int,un entier qui indique la position de l'histoire choisie par l'utilisateur.
*@param index   int,un entier qui indique la position de la partie 2 de l'histoire choisie par l'utilisateur.
*@param p1   Phrase,la phrase précédante à partir de laquelle on calcul la distance.
*@param p    Phrase,l'ancienne phrase la plus proche que l'on ne veut pas réinclure.
*
*@return Sommet_p1   map<Phrase, double>,le map contenant les distances relative à une phrase.
*/
map<Phrase, double> Distance_map2(vector<Histoire *> *hist, Phrase p, Phrase p1, int index,int index2)
{
    map<Phrase, double> Sommet_p1;
    for (auto it = (hist->at(index)->begin()); it != hist->at(index)->end(); ++it)
        if (it->Get_phrase() != p.Get_phrase())
            Sommet_p1[*it] = p1.distance(*it);
    for (auto it2 = (hist->at(index2)->begin()); it2 != hist->at(index2)->end(); ++it2)
        if (it2->Get_phrase() != p.Get_phrase())
            Sommet_p1[*it2] = p1.distance(*it2);
    return Sommet_p1;
}